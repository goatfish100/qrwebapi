package main

import (
	"net/http"

	"github.com/gorilla/mux"
)

//NewRouter - new Gorilla Router
func NewRouter() *mux.Router {
	//corsObj := handlers.AllowedOrigins([]string{"*"})
	r := mux.NewRouter()

	r.HandleFunc("/", Index)
	r.HandleFunc("/qrresource/{resourceuuid}", GetResource)
	r.HandleFunc("/deleteqrresource/{resourceuuid}", DeleteResource)
	r.HandleFunc("/qrresources", GetResources)

	r.HandleFunc("/createresource", ResourceCreate)
	r.HandleFunc("/updateresource", ResourceUpdate)
	//ResourceUpdate(w http.ResponseWriter, r *http.Request)
	r.HandleFunc("/getorgs", GetOrgs)
	r.HandleFunc("/getorg/{orgid}", GetOrg)
	r.HandleFunc("/createorg", PostCreateOrg)
	r.HandleFunc("/adduser/{orgid}", PostCreateUser)
	r.HandleFunc("/test", TestHandler)
	r.HandleFunc("/testjson", TestJsonHandler)
	r.HandleFunc("/createrfidcode", CreateRFIDCode)
	r.HandleFunc("/upload/{uuid}", Upload)

	r.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir("./static"))))
	return r
}
