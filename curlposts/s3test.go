package main

import (
	"log"
	"os"

	"github.com/minio/minio-go"
)

func main() {
	// Note: YOUR-ACCESSKEYID, YOUR-SECRETACCESSKEY, my-bucketname, my-objectname
	// and my-filename.csv are dummy values, please replace them with original values.

	// Requests are always secure (HTTPS) by default. Set secure=false to enable insecure (HTTP) access.
	// This boolean value is the last argument for New().

	// New returns an Amazon S3 compatible client object. API compatibility (v2 or v4) is automatically
	// determined based on the Endpoint value.
	s3Client, err := minio.New("s3.amazonaws.com", os.Getenv("AWS_KEY"), os.Getenv("AWS_PASSPHRASE"), true)
	if err != nil {
		log.Fatalln(err)
	}

	if _, err := s3Client.FPutObject("jltestingnumber3", "my-objectname", "my-testfile", "application/csv"); err != nil {
		log.Fatalln(err)
	}
	log.Println("Successfully uploaded my-filename.csv")
}
