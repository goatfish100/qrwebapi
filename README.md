###QR HELPER Application - is a collection of Micro Services #
#QRWEBAPI is the Web Api portion #
#QRRouter is the router directing traffic to QR Resource

#Installing/Running
Please have the latest golang installed (currently 1.8+) installed.  This should run on 1.7 and possibly 1.6


To run the tests - have mongo installed.  The test framework spawns a test instance that is created/destroyed on the fly.
On my PC - I have mongod setup but the daemon isn't running.  I use a Docker instance for Mongod

go test ./
OR  (for verbose output)
go test -v ./


QR Code Generation:
QR Codes code was added - it was previously in a seperate docker image file.  
this can used in the following manner
http://localhost:PORT/createrfidcode?data=Hello%2C%20world&size=300


RUNNING:
qrwebapi - can be run two ways - in Docker and orchestrated with docker-compose or with go with Mongo running on
locally or in Docker.

docker-compose repository is located at
https://bitbucket.org/goatfish100/docker-compose


Running Locally
set two environmental variables - like this  
export MONGO_HOST=localhost
export QRWEBAPI_PORT=8005
then run
./runprogram.sh


Dependencies/Building:
godeps is not used for package management and versioning -
In this folder there is GoDeps which has a json file with versions
and vendor file where the libraries are stored.

saving Dependencies
'godep save'

'godep go install' to build