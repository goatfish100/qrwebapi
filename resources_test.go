package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/qrhelper/qrhelperdatastructs"
	"gopkg.in/mgo.v2/bson"
)

var invokeCount int

func TestGetResource(t *testing.T) {
	t.Parallel()
	t.Log("TestGetResource Test")

	path := "/qrresource/" + resource1.Uuid
	t.Log("TestGetResource the path is ", path)
	r, _ := http.NewRequest("GET", path, nil)
	w := httptest.NewRecorder()

	InvokeHandler(http.HandlerFunc(GetResource), "/qrresource/{resourceuuid}", w, r)
	assert.Equal(t, http.StatusOK, w.Code)

	var res datastructs.Resource
	t.Log("the return string is bbxx", string(w.Body.Bytes()))

	if err = json.Unmarshal(w.Body.Bytes(), &res); err != nil {
		t.Log("unable to unmarshall json from getorgs")
		t.Fail()
	}

	t.Log("testgetresource xxx", res)
	if testResource(res, resource1) == false {
		t.Log("testOrg failure")
		t.Fail()
	}

}
func TestPutResource(t *testing.T) {
	t.Parallel()
	t.Log("TestPostResource Test")
	//resourcePost

	b, errPostOrg := json.Marshal(resourcePost)
	if errPostOrg != nil {
		t.Fail()
	}

	path := "/qrresource/" + resourcePost.Uuid

	r, _ := http.NewRequest("POST", path, bytes.NewReader(b))
	w := httptest.NewRecorder()
	w.Header()["Content-Type"] = []string{"Contect-Type: application/json"}

	InvokeHandler(http.HandlerFunc(GetResource), "/qrresource/{resourceuuid}", w, r)

	assert.Equal(t, http.StatusOK, w.Code)

	//var res datastructs.Resource
	// t.Log("the return string is bbxx", string(w.Body.Bytes()))
	//
	// if err = json.Unmarshal(w.Body.Bytes(), &res); err != nil {
	// 	t.Log("unable to unmarshall json from getorgs")
	// 	t.Fail()
	// }
	//
	// t.Log("testgetresource xxx", res)
	// if testResource(res, resource1) == false {
	// 	t.Log("testOrg failure")
	// 	t.Fail()
	// }

}
func TestGetResources(t *testing.T) {
	t.Parallel()
	t.Log("TestGetResources Test")

	path := "/qrresources"
	r, _ := http.NewRequest("GET", path, nil)
	w := httptest.NewRecorder()

	InvokeHandler(http.HandlerFunc(GetResources), "/qrresources", w, r)

	assert.Equal(t, http.StatusOK, w.Code)

	var res DataResources
	t.Log("the return string is ", string(w.Body.Bytes()))
	// check the return value
	if err = json.Unmarshal(w.Body.Bytes(), &res); err != nil {
		t.Log("unable to unmarshall json from getorgs")
		t.Fail()
	}
	//
	if testResource(res.Data[0], resource1) == false {
		t.Log("testOrg failure")
		t.Fail()
	}
	if testResource(res.Data[1], resource2) == false {
		t.Log("testOrg failure")
		t.Fail()
	}

}

func TestMGOGetResource(t *testing.T) {
	t.Log("TestGetResource Test")
	c := mgosession.DB(MongoDBDatabase).C("res")
	result := datastructs.Resource{}
	err = c.Find(bson.M{"uuid": "059edd7c-d454-11e6-92b9-374c2fc3d623"}).One(&result)

	t.Log("TestGetResource test address ", result.Address)
	if err != nil {
		t.Log(err)
	}
	if testResource(result, resource2) == false {
		t.Log("v")
	}

	// Try to find a resource that DOES NOT exist ... and ensure it isn't found
	result2 := datastructs.Resource{}
	err = c.Find(bson.M{"uuid": "does-not-exist"}).One(&result2)
	if result2.Address == "" {
		t.Log(result)
	} else {
		t.Fatal("mongo resource not equal")
	}
	//return result
}

func testResource(r1 datastructs.Resource, r2 datastructs.Resource) bool {
	var resflag bool
	if r1.Action == r2.Action &&
		r1.Address == r2.Address &&
		r1.Description == r2.Description &&
		r1.Protected == r2.Protected &&
		r1.Uuid == r2.Uuid {
		resflag = true
	} else {
		resflag = false
	}

	return resflag

}
