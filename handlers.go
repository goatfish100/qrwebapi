package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"image/png"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"

	"gopkg.in/mgo.v2/bson"

	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/qr"

	datastructs "gitlab.com/qrhelper/qrhelperdatastructs"
)

//FileUploadName - html form tag name
const FileUploadName = "upload"

//Index - sample index/test page
func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Welcome!\n")
}

type DataResources struct {
	Data datastructs.Resources `json:"resources"`
}

//GetResource - return information about a resource
func GetResource(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	var resourceUUID = vars["resourceuuid"]

	fmt.Println("GetResource resource id ", resourceUUID)
	res := RepoFindResource(resourceUUID)
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	fmt.Println("the resource is ", res.ID)
	if res.ID != "" {
		if err := json.NewEncoder(w).Encode(res); err != nil {
			log.Println("GetResource error encoding json", err)
			panic(err)
		}
		return
	}
	// If we didn't find it, 404
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusNotFound)
	if err := json.NewEncoder(w).Encode(jsonErr{Code: http.StatusNotFound, Text: "Not Found"}); err != nil {
		log.Println("GetResource error encoding success", err)
		panic(err)
	}
}

//GetResource - return information about a resource
func DeleteResource(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	var resourceUUID = vars["resourceuuid"]

	fmt.Println("DeleteResource resource id ", resourceUUID)
	res := RepoFindResource(resourceUUID)
	if strings.Index(res.Action, "s3:") == 0 {
		DelS3File(res.Address)
	}

	MongoDeleteResource(resourceUUID)
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	if err := json.NewEncoder(w).Encode(jsonErr{Code: http.StatusOK, Text: "Record Deleted"}); err != nil {
		log.Println("GetResource error encoding success", err)
		panic(err)
	}
	//log.Println("the resource is ", res.ID)
}

//GetResources - return information about a resource
func GetResources(w http.ResponseWriter, r *http.Request) {
	rsources := DataResources{}

	res := RepoFindResources()
	rsources.Data = res
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	//fmt.Println(res)
	if err := json.NewEncoder(w).Encode(rsources); err != nil {
		log.Println("GetResource error encoding json", err)
		panic(err)
	}
	return
	// If we didn't find it, 404
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(http.StatusNotFound)
	if err := json.NewEncoder(w).Encode(jsonErr{Code: http.StatusNotFound, Text: "Not Found"}); err != nil {
		log.Println("GetResource error encoding success", err)
		panic(err)
	}

}

//GetOrg return all orgs
func GetOrg(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	log.Printf("Org identifier%s\t ", vars["orgid"])
	idQueryier := bson.ObjectIdHex(vars["orgid"])
	log.Printf("Org identifier%s\t ", idQueryier)

	org := FindOrg(idQueryier)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	fmt.Println("inside GetOrg ", org)
	// check if empty array ...
	if org.ID == "" {
		error := jsonErr{Code: 404, Text: "Not Found"}
		if err := json.NewEncoder(w).Encode(error); err != nil {
			log.Println("GetOrg error encoding json", err)
			panic(err)
		}
	} else if err := json.NewEncoder(w).Encode(org); err != nil {
		log.Println("GetOrg error encoding object to json", err)
	}

}

//GetOrgs - get organizations refactor into one func ...
func GetOrgs(w http.ResponseWriter, r *http.Request) {

	c := mgosession.DB(MongoDBDatabase).C("orgusers")
	var orgs datastructs.Orgs

	err = c.Find(nil).All(&orgs)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	log.Println("inside GetOrgs", orgs)
	if err := json.NewEncoder(w).Encode(orgs); err != nil {
		log.Println("GetOrgs error encoding json", err)
		panic(err)
	}
	return
}

//PostCreateOrg - a post operation to create organization
func PostCreateOrg(w http.ResponseWriter, r *http.Request) {
	var organization datastructs.Org
	log.Println("PostCreateOrg r.body ", r.Body)
	decoder := json.NewDecoder(r.Body)
	log.Println("decoder ", decoder)
	err := decoder.Decode(&organization)
	if err != nil {
		log.Println("PostCreateOrg error reading input", err)
		panic(err)
	}

	OrgCreate(organization)
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	var jsonsuccess = datastructs.JSONSuccess{Success: "true"}

	if err := json.NewEncoder(w).Encode(jsonsuccess); err != nil {
		log.Println("PostCreateOrg error encoding json", err)
		panic(err)
	}
	fmt.Println("org is ", organization.ID)

}

//PostCreateUser - A Post Operation to create a user
func PostCreateUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	log.Println("PostCreateUser orgid is", vars["orgid"])
	var user datastructs.User
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&user)
	if err != nil {
		log.Println("PostCreateOrg error reading input", err)
		panic(err)
	}

	res := UserCreate(user, bson.ObjectIdHex(vars["orgid"]))
	if res == jsonSuccess {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		var jsonresp = datastructs.JSONSuccess{Success: "true"}

		if err = json.NewEncoder(w).Encode(jsonresp); err != nil {
			log.Println("PostCreateUser error encoding json sucess", err)
			panic(err)
		}
	} else {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		var jsonfailure = datastructs.JSONFailure{Failure: jsonFailure, Reason: res, Error: "Unable to insert"}

		if err = json.NewEncoder(w).Encode(jsonfailure); err != nil {
			log.Println("PostCreateUser error encoding json sucess", err)
			panic(err)
		}
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

}

//TestHandler - a test handler - hello Gorilla
func TestHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("inside test handler")
	w.Write([]byte("QR Web API test URL - Gorilla!\n"))
}

//TestJsonHandler is a simple I'am up
func TestJsonHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(200)
	var jsonresp = datastructs.JSONSuccess{Success: "true"}
	log.Println("TestJsonHandler called - logging")
	if err = json.NewEncoder(w).Encode(jsonresp); err != nil {
		log.Println("ResourceCreate error encoding json sucess", err)
		panic(err)
	}
}

//ResourceCreate - create a resource
func ResourceCreate(w http.ResponseWriter, r *http.Request) {
	var resource datastructs.Resource
	log.Println("ResourceCreate - entered", r.Body)
	decoder := json.NewDecoder(r.Body)

	if err != nil {
		log.Println("ResourceCreate error reading input", err)
	}

	err = decoder.Decode(&resource)
	if err != nil {
		log.Println("error decoding json", err)
		panic(err)
	}
	if err == nil {
		log.Println("ResourceCreate - resource decoded")
		log.Println(resource.Address)
		log.Println(resource.Description)
		log.Println(resource.Email)
		log.Println(resource.Name)
		log.Println("ResourceCreate - resource decoded")

		if resource.Uuid == "" {
			resource.Uuid = uuid.NewV4().String()
		}
		if ResourceInsert(resource) == true {
			w.Header().Set("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusOK)
			jsonresp := datastructs.ResourceSuccess{Success: "true", Uuid: resource.Uuid}
			log.Println(jsonresp)
			if err = json.NewEncoder(w).Encode(jsonresp); err != nil {
				log.Println("ResourceCreate error encoding json sucess", err)
				panic(err)
			}
		}

	} else {
		log.Println("Unable to decode resource")
		log.Println(err)
	}

	log.Println(resource.Address, resource.Email)
}

//ResourceUpdate - update a resource
func ResourceUpdate(w http.ResponseWriter, r *http.Request) {
	var resource datastructs.Resource
	log.Println("ResourceUpdate- entered", r.Body)
	decoder := json.NewDecoder(r.Body)
	if err != nil {
		log.Println("ResourceUpdate error reading input", err)
		//panic(err)
	}

	if err := decoder.Decode(&resource); err != nil {
		log.Println("ResourceUpdate - resource decoded")
		log.Println("Printing Resouce", resource.Uuid, resource.Address)
		log.Println("resources ...", resource.Uuid, resource.Address, resource.Description, resource.ID, resource.OrgId, resource.Protected)
		if MongoResourceUpdate(resource) == true {
			w.Header().Set("Content-Type", "application/json; charset=UTF-8")
			w.WriteHeader(http.StatusOK)
			jsonresp := datastructs.ResourceSuccess{Success: "true", Uuid: resource.Uuid}

			if err = json.NewEncoder(w).Encode(jsonresp); err != nil {
				log.Println("ResourceUpdate error encoding json sucess", err)
			}
		} else {
			w.WriteHeader(http.StatusOK)
			jsonresp := datastructs.ResourceSuccess{Success: "false", Uuid: resource.Uuid}

			if err = json.NewEncoder(w).Encode(jsonresp); err != nil {
				log.Println("ResourceUpdate error encoding json error", err)
			}
		}

	} else {
		log.Println("ResourceUpdate error decoding json", err)
		jsonresp := datastructs.ResourceSuccess{Success: "false", Uuid: resource.Uuid}
		if err = json.NewEncoder(w).Encode(jsonresp); err != nil {
			log.Println("ResourceUpdate error encoding json error", err)
		}
	}

	log.Println("ResourceUpdate updated resource", resource.Address)
}

func Upload(w http.ResponseWriter, r *http.Request) {
	log.Println("Upload method:", r.Method)
	vars := mux.Vars(r)

	var uuid = vars["uuid"]
	r.ParseMultipartForm(32 << 20)
	//r.ParseMultipartForm(0)

	file, handler, err := r.FormFile(FileUploadName)
	if err != nil {
		fmt.Println("error found", err)
		return
	}
	defer file.Close()
	fmt.Fprintf(w, "%v", handler.Header)
	storageName := uuid
	storagePathName := "/tmp/" + storageName

	log.Println("storagePathName .. " + storagePathName)
	f, err := os.OpenFile(storagePathName, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer f.Close()
	io.Copy(f, file)
	if PutS3File(storageName, storagePathName) == true {
		// Set some session values.
		log.Println("calling PutS3File", storageName)
		session, err := CookieStore.Get(r, os.Getenv("COOKIE_SECRET"))
		if err != nil {
			panic(err)
		}

		session.Values["fileName"] = handler.Filename
		session.Values["S3fileName"] = storageName
		session.Save(r, w)
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

}

func CreateRFIDCode(w http.ResponseWriter, r *http.Request) {
	log.Println("inside CreateRFIDCode")
	data := r.URL.Query().Get("data")
	if data == "" {
		http.Error(w, "", http.StatusBadRequest)
		return
	}

	s, err := url.QueryUnescape(data)
	if err != nil {
		http.Error(w, "", http.StatusBadRequest)
		return
	}

	code, err := qr.Encode(s, qr.L, qr.Auto)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	size := r.URL.Query().Get("size")
	if size == "" {
		size = "250"
	}

	intsize, err := strconv.Atoi(size)
	if err != nil {
		intsize = 250
	}

	// Scale the barcode to the appropriate size
	code, err = barcode.Scale(code, intsize, intsize)
	if err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	buffer := new(bytes.Buffer)
	if err := png.Encode(buffer, code); err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}

	download := r.URL.Query().Get("download")
	if download == "true" {
		attachmnt := "\"attachment; filename=" + s + ".png\""
		log.Println("download is true", attachmnt)
		w.Header().Set("Content-Disposition", attachmnt)
		w.Header().Set("Content-Type", "image/png")
	} else {
		w.Header().Set("Content-Type", "image/png")
	}
	w.Header().Set("Content-Length", strconv.Itoa(len(buffer.Bytes())))
	if _, err := w.Write(buffer.Bytes()); err != nil {
		http.Error(w, "", http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)

	log.Println("CreateRFIDCode finnished")
	fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[1:])
}
