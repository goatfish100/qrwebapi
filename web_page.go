package main

import (
	"log"
	"net/http"

	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/sessions"
	datastructs "gitlab.com/qrhelper/qrhelperdatastructs"
	mgo "gopkg.in/mgo.v2"
)

//MongoDBDatabase database
var MongoDBDatabase = "resources"

var mongohost = os.Getenv("MONGO_HOST")
var mgosession, err = mgo.Dial(mongohost)

const errOrgIDNotFound string = "Organization ID is not found"
const errUserNotAdded string = "User not added"
const jsonSuccess string = "Success"
const jsonFailure string = "Failure"

//AwsURL - Amazon AWS url
var AwsURL = os.Getenv("AWS_URL")

//AwsKey - Amazon AWS key
var AwsKey = os.Getenv("AWS_KEY")

//AwsPassPhrase - Amazon AWS passphrase
var AwsPassPhrase = os.Getenv("AWS_PASSPHRASE")

//AwsBucket - Amazon AWS Bucket
var AwsBucket = os.Getenv("AWS_BUCKET")

//var mgosession mgo.Session

//var qrresource QRResource
var orgs datastructs.Orgs
var users datastructs.Users

//var jsonsucess JsonSucess
//CookieStore - the cookie store/encrypt phrase
var CookieStore = sessions.NewCookieStore([]byte(os.Getenv("COOKIE_SECRET")))

// Give us some seed data
func insertRecords() {

	OrgCreate(datastructs.Org{
		Orgname:    "Rest Holdings",
		Address:    "123 H street",
		City:       "Culver",
		State:      "CA",
		Postalcode: "84109",
		Users: []datastructs.User{{
			Username: "freegyg",
			Email:    "freddy@yahoo.com",
			Name:     "Freddy G Spot",
			Password: "lsls",
		}, {
			Username: "toyo",
			Email:    "lsl@yahoo.com",
			Name:     "asdf",
			Password: "asdf",
		},
		}})
	OrgCreate(datastructs.Org{
		Orgname:    "awake Holdings",
		Address:    "123 H street",
		City:       "Culver",
		State:      "CA",
		Postalcode: "84109",
		Users: []datastructs.User{{
			Username: "freegyg",
			Email:    "freddy@yahoo.com",
			Name:     "Freddy G Spot",
			Password: "lsls",
		}, {
			Username: "toyo",
			Email:    "lsl@yahoo.com",
			Name:     "asdf",
			Password: "asdf",
		},
		},
	})

	ResourceInsert(datastructs.Resource{
		ID:          "1",
		Uuid:        "1232-1232-12312-123",
		OrgId:       "asdf",
		Description: "test resource",
		Protected:   "false",
		Action:      "forward",
		Address:     "https://surfhawaii.com",
	})

}

func main() {

	router := NewRouter()
	var portnumber = ":" + os.Getenv("QRWEBAPI_PORT")
	log.Println(portnumber)
	// Set up CORS
	corsObj := handlers.AllowedOrigins([]string{"*"})
	// and use combined logging handler as well
	log.Fatal(http.ListenAndServe(portnumber, handlers.CombinedLoggingHandler(os.Stdout, handlers.CORS(corsObj)(router))))
}
