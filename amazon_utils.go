package main

import (
	"log"

	minio "github.com/minio/minio-go"
)
func PutS3File(storeName string, filePath string) bool {
	log.Println("Inside PutS3File", AwsURL, AwsKey, AwsPassPhrase)
	minioClient, errs3 := minio.New(AwsURL, AwsKey, AwsPassPhrase, true)
	if errs3 != nil {
		log.Println("PutS3File error openning minioClient", err)
		panic(errs3)
	}

	// Upload the file with FPutObject
	log.Println("S3Bucket", AwsBucket)
	n, errs32 := minioClient.FPutObject(AwsBucket, storeName, filePath, "pdf")

	if errs32 != nil {
		//return false
		panic(errs32)
	}

	log.Println("AwsBucket ", AwsBucket)
	log.Printf("Successfully uploaded %s of size %d\n", storeName, n)
	return true
}

func DelS3File(storeName string) bool {
	log.Println("Inside PutS3File", AwsURL, AwsKey, AwsPassPhrase)
	minioClient, errs3 := minio.New(AwsURL, AwsKey, AwsPassPhrase, true)
	if errs3 != nil {
		log.Println("PutS3File error openning minioClient", err)
		panic(errs3)
	}

	err := minioClient.RemoveObject(AwsBucket, storeName)
	if err != nil {
		log.Println(err)
		return false
	}

	log.Println("AwsBucket ", AwsBucket)
	log.Printf("Successfully deleted ", storeName)
	return true
}
