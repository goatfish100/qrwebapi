package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"

	datastructs "gitlab.com/qrhelper/qrhelperdatastructs"

	"gopkg.in/mgo.v2/bson"
)

//invokeCount := 0

func TestFindOrg(t *testing.T) {
	t.Parallel()
	t.Log("TestFindOrg Test")
	t.Log(TestorgID1)
	org := FindOrg(TestorgID1)
	if org.Address == "" && org.Orgname == "" {
		t.Log("Empty return from mongo")
		t.Fail()
	}
	t.Log("finding org id", org.Address)

	//This is appending one more users in the structure
	org.Users = append(org.Users, datastructs.User{
		Username: "freegyg",
		Email:    "freddy@yahoo.com",
		Name:     "Freddy G Spot",
		Password: "lsls",
	})

	c := mgosession.DB(MongoDBDatabase).C("orgusers")
	//THis is updating the record
	err = c.Update(bson.M{"_id": TestorgID1}, org)
}

func TestGetOrgs(t *testing.T) {
	t.Parallel()
	t.Log("TestGetOrgs Test")

	r, _ := http.NewRequest("GET", "/getorgs", nil)
	w := httptest.NewRecorder()

	InvokeHandler(http.HandlerFunc(GetOrgs), "/getorgs", w, r)

	assert.Equal(t, http.StatusOK, w.Code)

	t.Log("rr code ", w.Code)
	//fmt.Println(rr.Result())
	var orgs datastructs.Orgs
	t.Log("the return string is ", string(w.Body.Bytes()))

	if err = json.Unmarshal(w.Body.Bytes(), &orgs); err != nil {
		t.Log("unable to unmarshall json from getorgs")
		t.Fail()
	}

	if len(orgs) < 2 {
		t.Log("recordcound does not match")
		t.Fail()
	}

}

func TestGetOrg(t *testing.T) {
	t.Parallel()
	t.Log("TestGetOrg Test")

	path := "/getorg/" + TestorgID1.Hex()
	r, _ := http.NewRequest("GET", path, nil)
	w := httptest.NewRecorder()

	InvokeHandler(http.HandlerFunc(GetOrg), "/getorg/{orgid}", w, r)

	assert.Equal(t, http.StatusOK, w.Code)

	var org datastructs.Org
	t.Log("the return string is ", string(w.Body.Bytes()))

	if err = json.Unmarshal(w.Body.Bytes(), &org); err != nil {
		t.Log("unable to unmarshall json from getorgs")
		t.Fail()
	}

	if testOrg(org, Org1) == false {
		t.Log("testOrg failure")
		t.Fail()
	}

}

func TestPostCreateOrg(t *testing.T) {
	//t.Parallel()
	t.Log("TestPostCreateOrg Test")

	b, errPostOrg := json.Marshal(post1)
	if errPostOrg != nil {
		t.Fail()
	}

	r, _ := http.NewRequest("POST", "/createorg", bytes.NewReader(b))
	w := httptest.NewRecorder()
	w.Header()["Content-Type"] = []string{"Contect-Type: application/json"}

	InvokeHandler(http.HandlerFunc(PostCreateOrg), "/createorg", w, r)

	t.Log("XXXXXX Status code is ", w.Code)
	t.Log(w.Body)
	assert.Equal(t, http.StatusOK, w.Code)

	// var org datastructs.Org
	// t.Log("the return string is ", string(w.Body.Bytes()))
	//
	// if err = json.Unmarshal(w.Body.Bytes(), &org); err != nil {
	// 	t.Log("unable to unmarshall json from getorgs")
	// 	t.Fail()
	// }

}

func TestPostCreateResource(t *testing.T) {
	t.Parallel()
	t.Log("TestPostCreateResource Test")

	b, err := json.Marshal(resource1)
	if err != nil {
		t.Fail()
	}

	r, _ := http.NewRequest("POST", "/resourcecreate", bytes.NewReader(b))
	w := httptest.NewRecorder()
	w.Header()["Content-Type"] = []string{"Contect-Type: application/json"}

	InvokeHandler(http.HandlerFunc(ResourceCreate), "/resourcecreate", w, r)

	assert.Equal(t, http.StatusOK, w.Code)

	t.Log("TestPostCreateResource return string is ", string(w.Body.Bytes()))

}

func InvokeHandler(handler http.Handler, routePath string,
	w http.ResponseWriter, r *http.Request) {

	// Add a new sub-path for each invocation since
	// we cannot (easily) remove old handler
	invokeCount++
	router := mux.NewRouter()
	http.Handle(fmt.Sprintf("/%d", invokeCount), router)

	router.Path(routePath).Handler(handler)

	// Modify the request to add "/%d" to the request-URL
	r.URL.RawPath = fmt.Sprintf("/%d%s", invokeCount, r.URL.RawPath)
	router.ServeHTTP(w, r)
}

func testOrg(r1 datastructs.Org, r2 datastructs.Org) bool {
	var resflag bool
	if r1.Address == r2.Address &&
		r1.City == r2.City &&
		r1.Postalcode == r2.Postalcode &&
		r1.State == r2.State &&
		r1.Users != nil {
		resflag = true
	} else {
		resflag = false
	}

	return resflag

}
