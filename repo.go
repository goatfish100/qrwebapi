package main

import (
	"fmt"
	"log"

	datastructs "gitlab.com/qrhelper/qrhelperdatastructs"
	"gopkg.in/mgo.v2/bson"
)

//RepoFindResource - find resources by id
func RepoFindResource(uuid string) datastructs.Resource {
	c := mgosession.DB(MongoDBDatabase).C("res")
	result := datastructs.Resource{}
	err = c.Find(bson.M{"uuid": uuid}).One(&result)

	if err != nil {
		log.Println("Error RepoFindResource", err)
	}
	return result
}

//RepoFindResources - find resources
func RepoFindResources() datastructs.Resources {
	c := mgosession.DB(MongoDBDatabase).C("res")
	results := datastructs.Resources{}
	err = c.Find(nil).All(&results)

	if err != nil {
		log.Println("RepoFindResources error", err)
	}
	return results
}

//OrgCreate create a new organization
func OrgCreate(o datastructs.Org) datastructs.Org {
	// Insert Datas
	c := mgosession.DB(MongoDBDatabase).C("orgusers")
	i := bson.NewObjectId()
	fmt.Println("The id is ", i)
	o.ID = i
	err = c.Insert(o)

	if err != nil {
		log.Println("OrgCreate error", err)
		panic(err)
	}
	orgs = append(orgs, o)
	return o
}

//UserCreate ... create a new user
func UserCreate(user datastructs.User, orgid bson.ObjectId) string {
	// Insert Datas
	c := mgosession.DB(MongoDBDatabase).C("orgusers")
	var org datastructs.Org

	err = c.Find(bson.M{"_id": orgid}).One(&org)
	if err != nil {
		return errOrgIDNotFound
	}

	org.Users = append(org.Users, user)

	err = c.Update(bson.M{"_id": orgid}, org)
	if err != nil {
		log.Println("Unable to insert user", user, err)
		return errUserNotAdded
	}
	return jsonSuccess
}

//FindOrg ... find an organization
func FindOrg(orgid bson.ObjectId) datastructs.Org {
	// Insert Datas

	c := mgosession.DB(MongoDBDatabase).C("orgusers")
	var org datastructs.Org

	err = c.Find(bson.M{"_id": orgid}).One(&org)

	if err != nil {
		log.Println("FingOrg error", err)
		panic(err)
	}

	return org
}

//ResourceInsert Insert resource into database
func ResourceInsert(resource datastructs.Resource) bool {

	c := mgosession.DB("resources").C("res")
	err = c.Insert(resource)

	if err != nil {
		log.Println("Unable to insert resource", err)
		return false
	}
	return true
}

//MongoResourceUpdate Update resource into database
func MongoResourceUpdate(resource datastructs.Resource) bool {
	log.Println("Inside MongoResourceUpdate")
	c := mgosession.DB("resources").C("res")
	log.Println("resource uuid is ", resource.Uuid)
	colQuerier := bson.M{"uuid": resource.Uuid}
	err = c.Update(colQuerier, resource)
	if err != nil {
		log.Println(err)
		return false
	}

	return true
}
