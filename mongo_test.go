package main

import (
	"fmt"
	"log"
	"os"
	"testing"

	"io/ioutil"

	"gitlab.com/qrhelper/qrhelperdatastructs"

	"gopkg.in/mgo.v2/bson"
	"gopkg.in/mgo.v2/dbtest"
)

var ()

var resource1 = datastructs.Resource{
	Uuid:        "d7d3b6f8-fb75-11e6-a3d8-a775920b2bca",
	OrgId:       "58a8d8a82374d6b23a9df1af",
	Description: "resource 1 test",
	Name:        "name one",
	Protected:   "false",
	Action:      "forward",
	Address:     "https://www.aa.com",
}

var resource2 = datastructs.Resource{
	Uuid:        "d7d3b6f8-fb75-11e6-a3d8-a775920b2bcb",
	OrgId:       "58a4a9c2d7d4384c0f7fd143",
	Description: "resource 2 test",
	Name:        "name two",
	Protected:   "false",
	Action:      "forward",
	Address:     "https://www.southwest.com",
}

var resourcePost = datastructs.Resource{
	Uuid:        "d7d3b6f8-fb75-11e6-a3d8-a775920b2def",
	OrgId:       "58a4a9c2d7d4384c0f7fd143",
	Description: "resource post test",
	Name:        "name post",
	Protected:   "false",
	Action:      "forward",
	Address:     "https://www.southwest.com",
}

var TestorgID1 bson.ObjectId
var TestorgID2 bson.ObjectId
var Org1 datastructs.Org
var Org2 datastructs.Org

var post1 = datastructs.Org{
	Orgname:    "Orgtest",
	Address:    "123 H street",
	City:       "Culver",
	State:      "CA",
	Postalcode: "84109",
	Users: []datastructs.User{{
		Username: "freegyg",
		Email:    "freddy@yahoo.com",
		Name:     "Freddy G Spot",
		Password: "lsls",
	}, {
		Username: "froyo",
		Email:    "lsl@yahoo.com",
		Name:     "asdf",
		Password: "asdf",
	},
	}}

var Server dbtest.DBServer

func insertFixtures() {

	if err := mgosession.DB(MongoDBDatabase).C("res").Insert(resource1); err != nil {
		log.Fatal("Unable to insert test record resource1")
	}

	if err := mgosession.DB(MongoDBDatabase).C("res").Insert(resource2); err != nil {
		log.Fatal("Unable to insert test record resource2")
	}

	org1 := OrgCreate(datastructs.Org{
		Orgname:    "Rest Holdings",
		Address:    "123 H street",
		City:       "Culver",
		State:      "CA",
		Postalcode: "84109",
		Users: []datastructs.User{{
			Username: "freegyg",
			Email:    "freddy@yahoo.com",
			Name:     "Freddy G Spot",
			Password: "lsls",
		}, {
			Username: "toyo",
			Email:    "lsl@yahoo.com",
			Name:     "asdf",
			Password: "asdf",
		},
		}})

	TestorgID1 = org1.ID
	Org1 = org1

	org2 := OrgCreate(datastructs.Org{
		Orgname:    "awake Holdings",
		Address:    "123 H street",
		City:       "Culver",
		State:      "CA",
		Postalcode: "84109",
		Users: []datastructs.User{{
			Username: "freegyg",
			Email:    "freddy@yahoo.com",
			Name:     "Freddy G Spot",
			Password: "lsls",
		}, {
			Username: "toyo",
			Email:    "lsl@yahoo.com",
			Name:     "asdf",
			Password: "asdf",
		},
		}})

	Org2 = org2
	TestorgID1 = org1.ID
	TestorgID2 = org2.ID
}

func TestMain(m *testing.M) {
	//os.TempDir()
	fmt.Println("TestMain - routine")
	tempDir, _ := ioutil.TempDir("", "testing")
	Server.SetPath(tempDir)

	// My main session var is now set to the temporary MongoDB instance
	mgosession = Server.Session()
	insertFixtures()
	// Run the test suite

	fmt.Println("m.Run - start")
	retCode := m.Run()
	fmt.Println("m.Run - End")
	// Make sure we DropDatabase so we make absolutely sure nothing is left or locked while wiping the data and
	// close session

	mgosession.DB(MongoDBDatabase).DropDatabase()
	mgosession.Close()

	Server.Stop()

	fmt.Println("exiting test main")
	// call with result of m.Run()
	os.Exit(retCode)

}
