package main

import (
	"log"

	"gopkg.in/mgo.v2/bson"

	"gitlab.com/qrhelper/qrhelperdatastructs"
)

//InsertResource into database
func InsertResource(resource datastructs.Resource) {
	log.Println("InsertResource call")
	if err := mgosession.DB(MongoDBDatabase).C("res").Insert(resource); err != nil {
		log.Println("Unable to insert resource", resource)
		log.Println(err)
	}
}

//MongoDeleteResource into database
func MongoDeleteResource(uuid string) {
	log.Println("DeleteResource call")
	//TODO - delete the resource
	//collection.Remove(bson.M{"name": "Foo Bar"})
	if err := mgosession.DB(MongoDBDatabase).C("res").Remove(bson.M{"uuid": uuid}); err != nil {
		log.Println("Unable to delete", uuid)
		log.Println(err)
		//return false

	}
	//return true
}
