package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetCode(t *testing.T) {
	t.Parallel()
	//createrfidcode
	path := "/createrfidcode" + "?data=Helloworld"
	t.Log("555555 path is " + path)
	r, _ := http.NewRequest("GET", path, nil)
	w := httptest.NewRecorder()

	InvokeHandler(http.HandlerFunc(CreateRFIDCode), "/createrfidcode", w, r)
	t.Log("TestGetCode return code ", w.Code)
	t.Log("r.path.log" + r.URL.Path)

	t.Log("TestGetCode ", w.Code)
	t.Log("TestGetCode Body Bytes ", w.Body.Bytes())

	//assert.NoError(t, err, msgAndArgs)
	assert.NotNil(t, w.Body.Bytes(), "not null test")

	// does not return 404 -  returns image
	assert.Equal(t, http.StatusOK, w.Code)
}
